#!/usr/bin/python3

import psycopg2
import add
import display
import delete
import update
import nbSeanceParSpectacle
import horaireSeance
import nbBilletPourUneSeance
import membreAssociation
import informationsMembreExterieur


HOST = "tuxa.sme.utc"
USER = "nf18p074"
PASSWORD = "0KnpbtUH"
DATABASE = "dbnf18p074"
try:
    conn = psycopg2.connect("dbname='dbnf18p074' user='nf18p074' host='tuxa.sme.utc' password='0KnpbtUH'")
    cur = conn.cursor()
except psycopg2.OperationalError as e:
    print("Vous n'êtes pas connecté au VPN de l'UTC. Vérifier la connection au VPN de l'UTC avant de démarrer l'application")
    quit()


option=1
while option != 0:
    print("\n\n-------- MENU ---------")
    print("[1] Ajouter une donnée")
    print("[2] Afficher une table")
    print("[3] Supprimer une donnée")
    print("[4] Mettre à jour une donnée")
    print("[5] Nombre de séance par spectacle")
    print("[6] L'horaire d'une séance")
    print("[7] Le nombre de billet pour une séance")
    print("[8] Les membres d'une association")
    print("[9] Le téléphone d'un membre d'extérieur")
    print("[0] Quitter")
    choix = input("Entrer un numéro : ")
    option = int(choix)
    print("option = ", option)
    try:
        if option == 1:
            add.add(conn)
        elif option == 2:
            display.display(conn)
        elif option == 3:
            delete.delete(conn)
        elif option == 4:
            update.update(conn)
        elif option == 5:
            nbSeanceParSpectacle.execute(conn)
        elif option == 6:
            horaireSeance.execute(conn)
        elif option == 7:
            nbBilletPourUneSeance.execute(conn)
        elif option == 8:
            membreAssociation.execute(conn)
        elif option == 9:
            informationsMembreExterieur.execute(conn)
        elif option == 0:
            quit()
    except psycopg2.Error as err:
        print("----------------------------")
        print(err)
        print("----------------------------\nErreur lors de l'aboutissement de l'action désirée. Vérifier le nom de la table, que les éléments existent déjà (salles, billet) avant de les attribuer dans une autre table. Vérifier également que des guillemets sont mises pour les horaires. Les indications ci dessus retournent en anglais ce qui ne va pas dans la base de données.")

        conn.rollback()

conn.close()
