#!/usr/bin/python3

import psycopg2
from tabulate import tabulate

def update(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []


    # afficher les tables
    table = input("\nEntrer le nom de la table de la ou les données à modifier :\n(Association, Inscription, Role, Billet, CategorieBillet, MembreDeLUniversite, MembreExterieur, Batiment, Salle, Spectacle, Seance) \n-> ")

    # afficher la table sélectionnée pour se repérer
    print("\n")
    sql = "SELECT * FROM " + table + ";"
    cur.execute(sql)
    nb_field = len(cur.description)
    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))

    condition = input("Entrez la condition (par exemple nom='jean-marc' modifiera la ligne de jean-marc) : \n-> ")
    element = input("Entrez un élément et sa nouvelle valeur à modifier (par exemple nom='jean-eude') : \n-> ")
    q=""
    while(q !="q" and q != "'q'" and q != ""):
        q = input("Entrez un autre élément à modifier (par exemple age='7') (entrez 'q' pour quitter) : \n-> ")
        element = element + ", " + q
    sql = "UPDATE " + table + " SET " + element + " WHERE " + condition + ";"

    cur.execute(sql)
    conn.commit()
