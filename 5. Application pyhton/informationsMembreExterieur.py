#!/usr/bin/python3

import psycopg2
from tabulate import tabulate


def execute(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []

    # afficher la table Spectacle pour se repérer
    print("\n")
    sql = "SELECT nom, prenom FROM MembreExterieur;"
    cur.execute(sql)
    nb_field = len(cur.description)
    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))

    nomMembre = input("\nEntrez le nom du MembreExterieur dont vous voulez connaitre les informations : \n-> ")

    sql = "SELECT * FROM MembreExterieur WHERE nom = '" + nomMembre + "';"
    print(sql)
    cur.execute(sql)
    #conn.commit()
    row = cur.fetchone()
    print("\nnom :", row[3])
    print("\nprénom :", row[4])
    print("\norganisme affilié :", row[1])
    print("\ntéléphone : +33", row[2])

    #SELECT * FROM MembreExterieur WHERE nom = 1;
