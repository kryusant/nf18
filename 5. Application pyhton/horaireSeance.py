import psycopg2
from tabulate import tabulate


def execute(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []

    # afficher la table Spectacle pour se repérer
    print("\n")
    sql = "SELECT idseance, salle, spectacle, type, duree, anneedeparution FROM Seance S JOIN Spectacle Sp ON S.spectacle=Sp.idspectacle;"
    cur.execute(sql)
    nb_field = len(cur.description)
    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))

    seance = input("\nEntrez l'id de la séance dont vous voulez connaitre l'horaire : \n-> ")

    sql = "SELECT horaire FROM Seance WHERE idseance = " + seance + ";"
    #print(sql)
    cur.execute(sql)
    #conn.commit()
    print("\nCette séance est à %s.\n\n" % str(cur.fetchone()[0])[:5])
