#!/usr/bin/python3

import psycopg2
from tabulate import tabulate


def execute(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []

    # afficher la table Spectacle pour se repérer
    print("\n")
    sql = "SELECT idassociation, nom FROM Association;"
    cur.execute(sql)
    nb_field = len(cur.description)
    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))

    idAssociation = input("Entrez l'id de l'Association dont vous voulez connaitre les membres : \n-> ")

    sql = "SELECT M.numeroCIN,M.nom,M.prenom,M.statut,I.role FROM Inscription I JOIN MembreDeLUniversite M ON I.numeroCIN=M.numeroCIN WHERE I.association = " + idAssociation + ";"
    print("\n Voici les membres de l'association : \n")
    cur.execute(sql)
    #conn.commit()

    data_rows = []


    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))
