#!/usr/bin/python3

import psycopg2
from tabulate import tabulate


def execute(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []

    # afficher la table Spectacle pour se repérer
    print("\n")
    sql = "SELECT * FROM Spectacle;"
    cur.execute(sql)
    nb_field = len(cur.description)
    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))

    spectacle = input("Entrez l'id du spectacle dont vous voulez connaitre le nombre de séance(s) : \n-> ")

    sql = "SELECT COUNT(*) AS idseance FROM Seance WHERE spectacle = " + spectacle + ";"
    #print(sql)
    cur.execute(sql)
    #conn.commit()
    print("\nIl y a %s séance(s) pour ce spectacle.\n\n" % str(cur.fetchone()[0]))
