#!/usr/bin/python3

import psycopg2
from tabulate import tabulate



def add(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []

    # afficher les tables
    table = input("\nEntrer le nom de la table où ajouter des données :\n(Association, Inscription, Role, Billet, CategorieBillet, MembreDeLUniversite, MembreExterieur, Batiment, Salle, Spectacle, Seance) \n-> ")

    # afficher la table sélectionnée pour se repérer
    print("\n")
    sql = "SELECT * FROM " + table + ";"
    cur.execute(sql)
    nb_field = len(cur.description)
    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))

    choice = "o"
    print("Entrez les données à ajouter dans cette table : ")
    while (choice == "o" or choice == "O"):
        data_to_insert = []
        for attribute in range(len(column_names)):
            display = column_names[attribute] + " : "
            data_to_insert.insert(attribute, input(display))

        attributes = ""
        for attribute in range(len(data_to_insert)):
            attributes = attributes + data_to_insert[attribute]
            if attribute != len(data_to_insert)-1:
                attributes = attributes + ", "
        choice = input("\nAjouter une autre donnée ? (o/n) ")

        sql = "INSERT INTO " + table + " VALUES (" + attributes + ");"

        cur.execute(sql)
        conn.commit()
