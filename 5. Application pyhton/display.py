#!/usr/bin/python3

import psycopg2
from tabulate import tabulate


def display(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []

    table = input("Entrer le nom de la table à afficher :\n(Association, Inscription, Role, Billet, CategorieBillet, MembreDeLUniversite, MembreExterieur, Batiment, Salle, Spectacle, Seance) \n-> ")
    choix = input("Afficher tout ? (o/n) ")
    if choix == "n" or choix == "N":
        choix = input("Sélectionner par colonnes ? (o/n) ")
        if choix == "o" or choix == "O":
            colonnes = ""
            colonnes += str(input("Quelle colonne ajouter à la sélection ?\n-> "))
            choix = input("Continuer à ajouter des colonnes ? (o/n) ")
            while choix == "o" or choix == "O":
                print("Quelle colonne ajouter à la sélection ?")
                colonnes += ", "
                colonnes += str(input())
                choix = input("Continuer à ajouter des colonnes ? (o/n) ")

        elif choix == "n" or choix == "N":
            colonnes = "*"
        choix = input("Sélectionner par lignes ? (o/n) ")
        if choix == "o" or choix == "O":
            lignes = " WHERE "
            print("Quelle condition de lignes ajouter à la sélection ?\n(ex: nom = ’Dupont’)\n(ex: id > 30)")
            lignes += str(input())
            choix = input("Continuer à ajouter des conditions de lignes à la sélection ? (o/n)")
            while choix == "o" or choix == "O":
                print("Quelle conditions de lignes ajouter à la sélection ?\n(ex: nom = ’Dupont’)\n(exemple : id > 30) \n-> ")
                lignes += " AND "
                lignes += str(input())
                choix = input("Continuer à ajouter des conditions de lignes à la sélection ? (o/n)")
        elif choix == "n" or choix=="N":
            lignes = ""
    elif choix == "o" or choix == "O":
        colonnes = "*"
        lignes = ""

    sql = "SELECT " + colonnes + " FROM " + table + lignes + ";"
    print(sql)
    cur.execute(sql)
    column_names = [desc[0] for desc in cur.description]
    nb_field = len(cur.description)

    for row in cur:
        data_rows.append(row)

    if not data_rows:
        print("La table n’existe pas")


    print (tabulate(data_rows, headers=column_names))

    #
    # row = cur.fetchone()
    # while row:
    #     print("\t[%s] %s (%s)" % (row[0],row[1],row[2]))
    #     row = cur.fetchone()
