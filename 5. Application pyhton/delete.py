#!/usr/bin/python3

import psycopg2
from tabulate import tabulate

def delete(conn):
    cur = conn.cursor()

    column_names = []
    data_rows = []


    # afficher les tables
    table = input("\nEntrer le nom de la table de la ou les données à supprimer :\n(Association, Inscription, Role, Billet, CategorieBillet, MembreDeLUniversite, MembreExterieur, Batiment, Salle, Spectacle, Seance) \n-> ")

    # afficher la table sélectionnée pour se repérer
    print("\n")
    sql = "SELECT * FROM " + table + ";"
    cur.execute(sql)
    nb_field = len(cur.description)
    column_names = [desc[0] for desc in cur.description]
    for row in cur:
        data_rows.append(row)

    print (tabulate(data_rows, headers=column_names))

    condition = input("Entrez la condition (par exemple nom='jean-marc' supprimera la ligne de jean-marc) : \n-> ")
    sql = "DELETE FROM " + table + " WHERE " + condition + ";"
    cur.execute(sql)
    conn.commit()
