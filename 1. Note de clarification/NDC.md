# Note de clarification

###### 1. Contexte

   1.1 Cahier des charges

   1.2 Hypothèses

###### 2. Objets

   2.1 Tableau synthétique avec propriétés et contraintes des objets

   2.2 Hypothèses sur les relation entre objets

###### 3. Utilisateurs 

###### 4. Acteurs du projet


# 1. Contexte

###### 1.1 Cahier des charges

L'université UTX est une université très active au niveau culturel et offre une grande variété de spectacles à son personnel et à ses étudiants. Pour mieux promouvoir les spectacles et permettre une meilleure organisation, l'université décide de créer une nouvelle base de données permettant d'enregistrer et gérer les spectacles organisés au niveau de l'université. L'université fait appel à vous pour la conception de cette nouvelle base et vous fourni les informations suivantes.

Les spectacles sont organisés par les associations culturelles de l'université. Chaque association a un nom (ex : « Dix de Chœur », « LIPON », « Mélo'dix »...), une description, une catégorie (théâtre, musique, danse, ...), un contact mail, une date de création et éventuellement un site web. On enregistre pour chaque association la liste des étudiants qui la composent avec pour information le numéro de CIN et les nom et prénom. Chaque membre a la possibilité de faire partie de plusieurs associations en même temps. Pour chaque association, deux membres ont un statut particulier qui doit être enregistré : le président et le trésorier. Chaque association a droit à une salle dans laquelle elle peut se réunir en fin de journée, qui a un numéro, un bâtiment, un type (salle de cours, bureau ou amphithéatre) et un nombre de personnes qu'elle peut recevoir.

Un spectacle organisé par une association peut être une pièce de théâtre, un concert ou un stand-up, qui ont tous une durée spécifique. Une pièce de théâtre a un auteur, une année de parution et un type (drame, comédie, etc.). Un concert a un compositeur, une année de parution et un genre (rock, classique, etc.). Un spectacle stand-up a un genre (spectacle comique, débat, table ronde). Pour un spectacle donné, des étudiants (pas forcément membres de l'association organisatrice) ou des personnels de l'université ont un rôle spécifique dans le spectacle (par exemple un rôle dans une pièce, ou un instrument dans un concert, ou un rôle d'animateur pour un débat). Un même spectacle peut être tenu dans différentes séances. Chaque séance a lieu a une date et une heure particulière, dans une des salles de l'université.

L'université souhaite aussi gérer les billets des spectacles. Pour chaque séance on peut générer plusieurs catégories de billets (« invitation », « billet étudiant », « billet extérieur », ...). Pour produire des billets pour un spectacle, on doit avoir les informations suivantes : catégorie, tarif, date création ainsi que le nombre de billets à produire dans cette catégorie. On veut également garder en mémoire qui a acheté des billets à une séance. Ce peut être un étudiant, un personnel de l'université (pour lequel on enregistrera son nom, prénom, numéro de CIN et statut : enseignant, personnel administratif ou personnel technique) ou un membre extérieur (pour lequel on enregistrera son nom, prénom, organisme d'affiliation (particulier pour ceux qui n'en ont pas) et un numéro de téléphone contact.

###### 1.2 Hypothèses

Pour simplifier le sujet, on considérera que :

* les personnes réalisant un spectacle donné ont toujours le même rôle dans ce spectacle (on ne considérera donc pas par exemple qu'une même pièce de théâtre puisse être réalisée par des personnes différentes) ;


# 2. Objets

###### 2.1 Tableau récapitulatif

Dans le tableau suivant seront détaillés les entités à traiter, leurs propriétés ainsi que leur contraintes:

**Objets**|**Propriétés**|**Contraintes**
:-----:|:-----:|:-----: 
Association|Nom, description, catégorie, email, date de création, site web, étudiants composants l'association|Il peut ne pas y avoir de site web ; L'association doit être composée de un étudiant minimum.|
Spectacle|Durée, type|Le type du spectacle doit être soit une pièce de théâtre, un concert, ou un stand-up|
Pièce de théâtre|Auteur, année de parution, type (drame, comédie, etc.)||
Concert|Compositeur, année de parution, genre (rock, classique, etc.)||
Stand-up|Genre| Le genre doit être soit un spectacle comique, un débat ou une table ronde.|
Séance|Date et heure, salle|La salle ne devra pas être un bureau.|
Salle|Numéro, bâtiment, type, nombre maximum de personnes toléré|Le type de salle doit être: salle de cours, bureau ou amphithéâtre.|
Personnel|Nom, prénom, numéro de CIN, statut|Le statut doit être enseignant, personnel administratif ou personnel technique.|
Billet|Catégorie, tarif, date de création, nombre de billet à produire dans une catégorie donnée, spectacle associé|La catégorie du billet n'est pas définie, ce peut être : invitation, billet étudiant, billet extérieur, etc..|
Spectateur|Statut|Le statut d'un spectateur doit être soit un étudiant, un personnel de l'université, ou un membre extérieur.|
Étudiant|Nom, Prénom, numéro de CIN||
Personnel|Nom, Prénom, numéro de CIN, statut|Le statut d'un personnel doit être soit enseignant, personnel administratif, ou personnel technique.|
Membre extérieur|Nom, prénom, organisme d’affiliation, numéro de téléphone|Si le membre extérieur n'a pas organisme d'affiliation,  alors cette propriété sera notée "particulier"|

###### 2.2 Clarification sur les relations entre objets

* une association doit être composée de un étudiant minimum ;
* Un étudiant peut avoir une, plusieurs ou aucune association en même temps ;
* Pour chaque association, deux membres ont un statut particulier : le trésorier et le président ;
* Chaque association a droit à une salle pour se réunir ;
* Au moins un spectacle est organisé par chaque association ;
* Pour un spectacle donné, des étudiants (pas forcément membres de l'association organisatrice) ou des personnels de l'université ont un rôle spécifique dans le spectacle ;
* Une séance a une salle et une salle peut accueillir plusieurs séances ou aucune ;
* Un spectacle peut avoir lieu dans différentes séances, une séance correspond à un seul spectacle.

# 3. Droits des utilisateurs

Nous n'avons aucune information concernant les personnes autorisées à contrôler les tables de la base de données. Nous demanderons donc ces informations au client.

# 4. Acteurs du projet

**Maître d'ouvrage** : Fatma Chamkeh

**Maîtres d'œuvre** : Mattéo Borrego, Myriem Khal, Antoine Kryus, Adel Saoud