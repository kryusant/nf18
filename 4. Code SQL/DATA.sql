INSERT INTO Batiment (nom) VALUES('BF');
INSERT INTO Batiment (nom) VALUES('Centre de recherche');
INSERT INTO Batiment (nom) VALUES('PG');
INSERT INTO Batiment (nom) VALUES('Victor Hugo');
INSERT INTO Batiment (nom) VALUES('Jean Rostand');
INSERT INTO Batiment (nom) VALUES('Henry Martin');
INSERT INTO Batiment (nom) VALUES('Marie Curie');

INSERT INTO Salle VALUES('425', 1, 'amphitheatre');
INSERT INTO Salle VALUES('122', 2, 'amphitheatre');
INSERT INTO Salle VALUES('21', 3, 'amphitheatre');
INSERT INTO Salle VALUES('501', 1, 'salle de cours');
INSERT INTO Salle VALUES('100', 4, 'salle de cours');
INSERT INTO Salle VALUES('102', 5, 'salle de cours');
INSERT INTO Salle VALUES('433', 6, 'bureau');
INSERT INTO Salle VALUES('56', 1, 'bureau');
INSERT INTO Salle VALUES('109', 7, 'bureau');
INSERT INTO Salle VALUES('78', 2, 'bureau');

INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('UTC Sport', 'asso de sport', 'sport collectif','utcsport@utc.fr','2010-04-02','utc.sport@utc.com','56');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('BDE', 'Bureau des etudiants', 'aide etudiant','bde@utc.fr','1976-09-16','bde@utc.com','100');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('Picasoft', 'Education populaire aux enjeux du numérique', 'aide etudiant','picasoft@utc.fr','2000-11-24',NULL,'100');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('Festupic', 'FESTival Universitaire de PICardie', 'EvenementUTC','festipic@utc.fr','2000-01-28','festupic@utc.fr','425');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('JDA', 'Journee des association', 'aide numerique','jda@utc.fr','1976-04-09','jda@utc.com','56');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('Comet', 'Comet est lAssociation qui organise des Estu pas piquées hannetons', 'EvenementUTC','comet@utc.fr','2013-04-18','comet@utc.com','102');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('Avocarotte', 'Promouvoir le mode de vie vegan', 'aide etudiant','avocarotte@utc.fr','2016-04-18','avocarotte@utc.com','433');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('La comedie musicale', 'La Comédie Musicale de lUTC', 'EvenementUTC','lacomediemusicale@utc.fr','2018-04-18','lacomediemusicale@utc.com','21');
INSERT INTO Association (nom,description,categorie,mail,dateDeCreation,siteWeb,salle) VALUES ('BDO', 'Les baignoires dans lOise', 'EvenementUTC','BDO@utc.fr','2007-12-10','BDO@utc.com','78');

INSERT INTO Spectacle (duree, auteur, typeTheatre, anneeDeParution, type) VALUES (121, 'David', 'Comedie', 1987, 'PieceDeTheatre');
INSERT INTO Spectacle (duree, auteur, typeTheatre, anneeDeParution, type) VALUES (189, 'Alicia', 'Tragedie', 2018, 'PieceDeTheatre');
INSERT INTO Spectacle (duree, auteur, typeTheatre, anneeDeParution, type) VALUES (121, 'François', 'Comedie', 2007, 'PieceDeTheatre');
INSERT INTO Spectacle (duree, auteur, typeTheatre, anneeDeParution, type) VALUES (90, 'Anna', 'Tragedie', 2004, 'PieceDeTheatre');
INSERT INTO Spectacle (duree, compositeur, anneeDeParution, genreConcert, type) VALUES (121, 'Harry', 1977, 'Rock','Concert');
INSERT INTO Spectacle (duree, compositeur, anneeDeParution, genreConcert, type) VALUES (107, 'Joel', 2002, 'Pop', 'Concert');
INSERT INTO Spectacle (duree, compositeur, anneeDeParution, genreConcert, type) VALUES (88, 'Marie', 2019, 'Metal', 'Concert');
INSERT INTO Spectacle (duree, genreStandUp, type) VALUES (30, 'table ronde', 'StandUp');
INSERT INTO Spectacle (duree, genreStandUp, type) VALUES (40, 'debat', 'StandUp');
INSERT INTO Spectacle (duree, genreStandUp, type) VALUES (190, 'spectacle comique', 'StandUp');

INSERT INTO Seance (horaire, spectacle, salle) VALUES ('2006-12-23 16:30:00', 1, 21);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('2006-09-12 10:30:00', 1, 122);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('1999-02-11 09:00:00', 1, 21);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('2019-12-04 16:30:00', 2, 100);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('2020-04-05 20:45:00', 2, 56);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('2008-09-12 16:30:00', 3, 100);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('2006-09-23 15:20:00', 4, 78);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('1977-06-18 10:00:00', 5, 433);
INSERT INTO Seance (horaire, spectacle, salle) VALUES ('2021-04-21 17:25:00', 6, 102);

INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Abel','Lounis','enseignant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Jean','Pascaline','enseignant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Younes','Khaled','personnel administratif');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Desproges','Sarah','personnel technique');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Rodrigo','Paul','enseignant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Borrego','Matteo','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Khal','Myriem','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Kryus','Antoine','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Cuadrado','Juan','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Sneyt','Morgane','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Ginter','Mat','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Platis','Samuel','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Fideret','Clémence','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Tabnini','Rachida','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Christensen','Hafsa','etudiant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Camara','Christiane','enseignant');
INSERT INTO MembreDeLUniversite (nom, prenom, statut) VALUES('Luis','Harry','etudiant');


INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('LaCaf',0649496591,'Logan','Rythme');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('Pole Emploi',0709769188,'Matias','Gunter');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('Areli',0645876610,'Rugani','Daniele');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('Edf',0987276591,'Fagioli','Nicolo');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('Smeno',0678980076,'Lo Celso','Giovanni');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('GDF',0712006671,'Forte','Ngolo');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('CNIL',0371986210,'Moussa','Doumbia');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('Banque de France',0678961898,'Santon','Davide');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('ONF',0761987500,'Felipe','Dias');
INSERT INTO MembreExterieur (organismeAffilie, telephone, nom, prenom) VALUES('OMC',0612569871,'Snitch','Margareth');

INSERT INTO CategorieBillet (nom, tarif, nombreDeBillet) VALUES ('universite',12,130);
INSERT INTO CategorieBillet (nom, tarif, nombreDeBillet) VALUES ('VIP',84,20);
INSERT INTO CategorieBillet (nom, tarif, nombreDeBillet) VALUES ('senior',8,14);
INSERT INTO CategorieBillet (nom, tarif, nombreDeBillet) VALUES ('exterieur',15,190);
INSERT INTO CategorieBillet (nom, tarif, nombreDeBillet) VALUES ('etudiant',3,200);

INSERT INTO Inscription VALUES(7,1,'president');
INSERT INTO Inscription VALUES(8,1,'tresorier');
INSERT INTO Inscription VALUES(9,1,'quelconque');
INSERT INTO Inscription VALUES(10,2,'president');
INSERT INTO Inscription VALUES(11,2,'tresorier');
INSERT INTO Inscription VALUES(12,2,'quelconque');
INSERT INTO Inscription VALUES(13,3,'president');
INSERT INTO Inscription VALUES(14,3,'tresorier');
INSERT INTO Inscription VALUES(15,3,'quelconque');
INSERT INTO Inscription VALUES(17,4,'president');


INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(8,8, 'animateur');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(4,5, 'acteur: principal');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(7,9, 'sono');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(6,6, 'guitariste');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(2,3, 'scenariste');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(3,14, 'acteur: la jeune fille en robe');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(6,16, 'sono');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(10,2, 'auteur');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(9,8, 'préparation');
INSERT INTO Role(idSpectacle, numeroCIN, role) VALUES(1,6, 'acteur: clown');

INSERT INTO Billet (dateDeCreation, acheteurUniversite, seance, categorieBillet) VALUES ('2021-03-16',10,7,5);
INSERT INTO Billet (dateDeCreation, acheteurUniversite, seance, categorieBillet) VALUES ('2022-04-17',9,4,2);
INSERT INTO Billet (dateDeCreation, acheteurUniversite, seance, categorieBillet) VALUES ('2023-05-18',7,1,5);
INSERT INTO Billet (dateDeCreation, acheteurUniversite, seance, categorieBillet) VALUES ('2024-06-19',16,8,3);
INSERT INTO Billet (dateDeCreation, acheteurUniversite, seance, categorieBillet) VALUES ('2025-07-20',14,5,5);
INSERT INTO Billet (dateDeCreation, acheteurUniversite, seance, categorieBillet) VALUES ('2026-08-21',6,4,1);
INSERT INTO Billet (dateDeCreation, acheteurUniversite, seance, categorieBillet) VALUES ('2027-09-22',8,2,1);
INSERT INTO Billet (dateDeCreation, acheteurExterieur, seance, categorieBillet) VALUES ('2028-10-23',1,5,1);
INSERT INTO Billet (dateDeCreation, acheteurExterieur, seance, categorieBillet) VALUES ('2029-11-24',8,7,1);
INSERT INTO Billet (dateDeCreation, acheteurExterieur, seance, categorieBillet) VALUES ('2030-12-25',5,3,5);
