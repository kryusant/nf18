CREATE DATABASE projet_nf18;
USE projet_nf18;

CREATE TABLE Batiment (
  idBatiment SERIAL PRIMARY KEY,
  nom VARCHAR(255) NOT NULL
);

CREATE TABLE Salle (
  numero INTEGER PRIMARY KEY,
  batiment INTEGER NOT NULL,
  type VARCHAR(255) NOT NULL CHECK (type ='salle de cours' OR
                               type ='bureau' OR
                               type ='amphitheatre'),
  FOREIGN KEY (batiment) REFERENCES Batiment(idBatiment)
);


CREATE TABLE Association (
  idAssociation SERIAL PRIMARY KEY,
  nom VARCHAR(255) NOT NULL,
  description VARCHAR(255) NOT NULL,
  categorie VARCHAR(255) NOT NULL,
  mail VARCHAR(255) NOT NULL,
  dateDeCreation DATE NOT NULL,
  siteWeb VARCHAR(255),
  salle INTEGER NOT NULL,
  FOREIGN KEY (salle) REFERENCES Salle(numero)
);

CREATE TABLE Spectacle (
  idSpectacle SERIAL PRIMARY KEY,
  duree INTEGER,
  auteur VARCHAR(255),
  anneeDeParution INTEGER,
  typeTheatre VARCHAR(255),
  compositeur VARCHAR(255),
  genreConcert VARCHAR(255),
  genreStandUp VARCHAR(255) CHECK (genreStandUp = 'spectacle comique' OR
                                       genreStandUp = 'debat' OR
                                       genreStandUp = 'table ronde'),
  type VARCHAR(255) NOT NULL CHECK (type = 'PieceDeTheatre' OR
                               type = 'Concert' OR
                               type = 'StandUp'),

  CHECK ((auteur IS NOT NULL AND anneeDeParution IS NOT NULL AND typeTheatre IS NOT NULL AND compositeur IS NULL AND genreConcert IS NULL AND genreStandUp IS NULL AND type = 'PieceDeTheatre')
  OR (auteur IS NULL AND anneeDeParution IS NOT NULL AND typeTheatre IS NULL AND compositeur IS NOT NULL AND genreConcert IS NOT NULL AND genreStandUp IS NULL AND type = 'Concert')
  OR (auteur IS NULL AND anneeDeParution IS NULL AND typeTheatre IS NULL AND compositeur IS NULL AND genreConcert IS NULL AND genreStandUp IS NOT NULL AND type = 'StandUp'))
);

CREATE TABLE Seance (
  idSeance SERIAL PRIMARY KEY,
  horaire TIME NOT NULL,
  spectacle INTEGER NOT NULL,
  salle INTEGER NOT NULL,
  FOREIGN KEY (spectacle) REFERENCES Spectacle(idSpectacle),
  FOREIGN KEY (salle) REFERENCES Salle(numero)
);

CREATE TABLE MembreDeLUniversite (
  numeroCIN SERIAL PRIMARY KEY,
  nom VARCHAR(255) NOT NULL,
  prenom VARCHAR(255) NOT NULL,
  statut VARCHAR(255) NOT NULL CHECK (statut = 'enseignant' OR
                                 statut = 'personnel administratif' OR
                                 statut = 'personnel technique' OR
                                 statut = 'etudiant')
);

CREATE TABLE MembreExterieur (
  idMembreExterieur SERIAL PRIMARY KEY,
  organismeAffilie VARCHAR(255) NOT NULL,
  telephone CHAR(10) NOT NULL,
  nom VARCHAR(255) NOT NULL,
  prenom VARCHAR(255) NOT NULL
);

CREATE TABLE CategorieBillet (
    idCategorie SERIAL PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    tarif INTEGER NOT NULL,
    nombreDeBillet INTEGER NOT NULL
);

CREATE TABLE Billet (
  idBillet SERIAL PRIMARY KEY,
  dateDeCreation DATE NOT NULL,
  acheteurUniversite INTEGER,
  acheteurExterieur INTEGER,
  seance INTEGER NOT NULL,
  categorieBillet INTEGER NOT NULL,
  FOREIGN KEY (acheteurUniversite) REFERENCES MembreDeLUniversite(numeroCIN),
  FOREIGN KEY (acheteurExterieur) REFERENCES MembreExterieur(idMembreExterieur),
  FOREIGN KEY (seance) REFERENCES Seance(idSeance),
  FOREIGN KEY (categorieBillet) REFERENCES CategorieBillet(idCategorie),
  CHECK ((acheteurUniversite IS NOT NULL AND acheteurExterieur IS NULL) OR (acheteurUniversite IS NULL AND acheteurExterieur IS NOT NULL))
);

CREATE TABLE Inscription (
  numeroCIN INTEGER NOT NULL,
  association INTEGER NOT NULL,
  FOREIGN KEY (numeroCIN) REFERENCES MembreDeLUniversite(numeroCIN),
  FOREIGN KEY (association) REFERENCES Association(idAssociation),
  role VARCHAR(255) NOT NULL CHECK (role = 'president' OR
                               role = 'tresorier' OR
                               role = 'quelconque')
);

CREATE TABLE Role (
  idSpectacle INTEGER NOT NULL,
  numeroCIN INTEGER NOT NULL,
  role VARCHAR(255) NOT NULL,
  FOREIGN KEY (idSpectacle) REFERENCES Spectacle(idSpectacle),
  FOREIGN KEY (numeroCIN) REFERENCES MembreDeLUniversite(numeroCIN)
);
