conn = new Mongo();
db = conn.getDB("nf18");

recordset1 = db.spectacles.find({"duree" : 121})
recordset2 = db.spectacles.find({"anneeDeParution": {$lt:1980}})
recordset3 = db.spectacles.find({"auteur": "François"})
recordset4 = db.spectacles.find({"seances.salle": 100})

printjson( {"Chercher les spectacles dont la durée est exactement 121 minutes": "réussi !"})

while ( recordset1.hasNext() ) {
   printjson( recordset1.next() );
}

printjson( {"Chercher les spectacles créés avant l'année 1980": "réussi !"})


while ( recordset2.hasNext() ) {
   printjson( recordset2.next() );
}

printjson( {"Chercher les spectacles dont l'auteur est François": "réussi !"})


while ( recordset3.hasNext() ) {
   printjson( recordset3.next() );
}
printjson( {"Chercher les spectacles dont les séances se produisent en salle 100": "réussi !"})
while ( recordset4.hasNext() ) {
   printjson( recordset4.next() );
}
