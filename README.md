# PROJET NF18

Vous trouverez dans ce dépôt :
* Une note de clarification (NDC.md)
* Un Modèle Conceptuel de Donnée en UML (MCD.png) ainsi que le code source en plantuml (MCD.uml)

# 

## Resources en ligne pour les membres:
- [Cloud du projet : compte-rendus de réunions, fichiers diverses](https://cloud.utc.fr/index.php/apps/files/?dir=/Shared/NF18)
- [Discord du projet](https://discord.gg/62G4DCC6kB)

## Tutoriel installation du git en local 
##### Commencez par télécharger le projet:
tapez les commandes successives dans le repertoire de votre choix:
```bash
git clone https://gitlab.utc.fr/kryusant/nf18.git
```
```bash
cd nf18 											#on se place dans le répertoire git
```
```bash
git config --global credential.helper store			#pour sauvergarder ses logins
```

##### Pour télécharger les mises à jour ("pull", il faut le faire avant TOUTE modification)
```bash 
git pull
```
##### Pour mettre à jour ses modifications ("commit" et "push")
```bash
git commit -a -m "message associé au commit"        # commit = sauvegarder en local (l'option -a veut dire all, vous pouvez mettre les noms des fichiers à commit à la place)
git push 											# pour uploader tous le code modifié (commited)
```

##### Pour ajouter tous les nouveaux fichiers créés localement au projet
```bash
git add * 										    # à faire avant le nouveau commit si le fichier est nouvellement créé
```
